// ATTENTION: Do not duplicate id.
export const inventory = [
  {
    id: 11,
    name: "DA22",
    type: "awning",
    winch: "band",
    tube: 50,
    front: 30,
    weave: 80
  },
  {
    id: 12,
    name: "DA22 nya konsollen",
    type: "awning",
    winch: "engine",
    tube: 50,
    front: 30,
    weave: 80
  },
  {
    id: 13,
    name: "DA22/VS24 överrullad",
    type: "screen",
    winch: "band",
    tube: 50,
    weight: 70,
    weave: 60
  },
  {
    id: 14,
    name: "DA22/VS24 underrullad",
    type: "screen",
    winch: "band",
    tube: 50,
    weight: 80,
    weave: 70
  },
  {
    id: 15,
    name: "DA22/VS24 underrullad nya konsollen",
    type: "screen",
    winch: "engine",
    tube: 40,
    weight: 70,
    weave: 60
  },
  {
    id: 21,
    name: "DA32",
    type: "screen",
    winch: "band",
    cassette: 15,
    tube: 60,
    front: 33,
    weave: 80
  },
  {
    id: 22,
    name: "DA32",
    type: "screen",
    winch: "engine",
    cassette: 15,
    tube: 49,
    front: 33,
    weave: 80
  },
  {
    id: 23,
    name: "DA32/VS34",
    type: "screen",
    winch: "band",
    cassette: 15,
    tube: 60,
    weight: 50,
    weave: 80
  },
  {
    id: 24,
    name: "DA32/VS34",
    type: "screen",
    winch: "engine",
    cassette: 15,
    tube: 49,
    weight: 60,
    weave: 80
  },
  {
    id: 31,
    name: "Bennströms",
    type: "awning",
    winch: "band",
    cassette: 10,
    tube: 55,
    front: 35,
    weave: 75
  },
  {
    id: 32,
    name: "Bennströms",
    type: "awning",
    winch: "engine",
    cassette: 10,
    tube: 43,
    front: 35,
    weave: 63
  },
  {
    id: 41,
    name: "Bennströms överrullad",
    type: "screen",
    winch: "band",
    cassette: 10,
    tube: 55,
    weight: 65,
    weave: 65
  },
  {
    id: 42,
    name: "Bennströms underrullad",
    type: "screen",
    winch: "band",
    cassette: 10,
    tube: 55,
    weight: 75,
    weave: 75
  },
  {
    id: 43,
    name: "Bennströms underrullad",
    type: "screen",
    winch: "engine",
    cassette: 15,
    tube: 39,
    weight: 48,
    weave: 48
  },
  {
    id: 51,
    name: "DA42",
    type: "awning",
    winch: "band",
    cassette: 25,
    tube: 82,
    front: 59,
    weave: 110
  },
  {
    id: 52,
    name: "DA42",
    type: "awning",
    winch: "gear",
    cassette: 25,
    tube: 88,
    front: 88,
    weave: 120
  },
  {
    id: 53,
    name: "DA42",
    type: "awning",
    winch: "engine",
    cassette: 25,
    tube: 78,
    front: 55,
    weave: 110
  },
  {
    id: 54,
    name: "DA42/VS44",
    type: "screen",
    winch: "band",
    cassette: 25,
    tube: 88,
    weight: 110,
    weave: 100
  },
  {
    id: 55,
    name: "DA42/VS44",
    type: "screen",
    winch: "engine",
    cassette: 25,
    tube: 78,
    weight: 100,
    weave: 90
  },
  {
    id: 61,
    name: "DA52",
    type: "awning",
    winch: "gear",
    tube: 110,
    front: 20,
    weave: 140
  },
  {
    id: 62,
    name: "DA52",
    type: "awning",
    winch: "engine",
    tube: 105,
    front: 20,
    weave: 140
  },
  {
    id: 71,
    name: "BS26 balkongmarkis",
    type: "awning",
    tube: 110,
    front: 10,
    weave: 130
  },
  {
    id: 81,
    name: "Lunex",
    type: "terrace",
    winch: "gear",
    tube: 130,
    front: 120,
    weave: 160,
    carriage: 25
  },
  {
    id: 82,
    name: "Lunex",
    type: "terrace",
    winch: "engine",
    tube: 130,
    front: 120,
    weave: 160,
    carriage: 25
  },
  {
    id: 91,
    name: "FA22",
    type: "terrace",
    winch: "gear",
    tube: 110,
    front: 110,
    weave: 140,
    carriage: 20
  },
  {
    id: 92,
    name: "FA22",
    type: "terrace",
    winch: "engine",
    tube: 100,
    front: 90,
    weave: 120,
    carriage: 20
  }
];

export default {
  inventory
};
