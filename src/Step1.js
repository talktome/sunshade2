import React from "react";

const Step1 = ({ state, change }) => {
  if (state.step !== 1) return null;
  return (
    <div>
      <h2>Step 1</h2>
      <Input change={change.bind(this)} />
      <p />
      <InventoryTable state={state} />
      <p />
      <input
        disabled={!state.ready1}
        name="continue"
        type="button"
        value="Continue"
        onClick={change.bind(this)}
      />
    </div>
  );
};

export default Step1;

const Input = ({ change }) => (
  <div>
    <h3>Input</h3>
    <table>
      <tbody>
        <tr>
          <td>Order:</td>
          <td>
            <input
              type="number"
              min="0"
              name="order"
              style={{ textAlign: "right" }}
              onChange={change.bind(this)}
            />
          </td>
        </tr>
        <tr>
          <td>Quantity:</td>
          <td>
            <input
              type="number"
              min="0"
              name="quantity"
              style={{ textAlign: "right" }}
              onChange={change.bind(this)}
            />
          </td>
        </tr>
        <tr>
          <td>Id:</td>
          <td>
            <input
              type="number"
              min="0"
              name="id"
              style={{ textAlign: "right" }}
              onChange={change.bind(this)}
            />
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);

const InventoryTable = ({ state }) => (
  <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Type</th>
        <th>Winch</th>
        <th>Cassette</th>
        <th>Tube</th>
        <th>Front</th>
        <th>Weight</th>
        <th>Weave</th>
        <th>Carriage</th>
      </tr>
    </thead>
    <tbody>
      {state.inventory.map(p => (
        <ProductTr state={state} key={p.id} product={p} />
      ))}
    </tbody>
  </table>
);

const ProductTr = ({ state, product }) => (
  <tr bgcolor={product === state.selected ? "yellow" : "white"}>
    <td align="right">{product.id}</td>
    <td>{product.name}</td>
    <td>{product.type}</td>
    <td>{product.winch}</td>
    <td align="right">{product.cassette}</td>
    <td align="right">{product.tube}</td>
    <td align="right">{product.front}</td>
    <td align="right">{product.weight}</td>
    <td align="right">{product.weave}</td>
    <td align="right">{product.carriage}</td>
    <td align="right">{product.note}</td>
  </tr>
);
