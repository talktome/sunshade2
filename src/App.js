import React from "react";
import { inventory } from "./inventory.js";
import Step1 from "./Step1";
import Step2 from "./Step2";
import "./App.css";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      inventory: inventory,
      selected: null,
      order: "",
      quantity: "",
      arm: "",
      height: "",
      width: "",
      step: 1,
      ready1: false
    };
  }

  change(e) {
    let newState = this.state;
    if (e.target.name === "id") {
      newState.selected =
        inventory.find(p => p.id.toString() === e.target.value) || null;
    } else if (e.target.name === "continue") {
      newState.step = 2;
    } else {
      newState[e.target.name] = e.target.value;
    }

    newState.ready1 =
      newState.order > 0 && newState.quantity > 0 && newState.selected !== null;
    this.setState(newState);
	console.log(this.state);
  }

  render() {
    return (
      <div>
        <h1>App</h1>
        <Step1 state={this.state} change={this.change.bind(this)} />
        <Step2 state={this.state} change={this.change.bind(this)} />
      </div>
    );
  }
}
