import React from "react";

const Step2 = ({ state, change }) => {
  if (state.step !== 2) return null;
  if (state.selected.type === "awning"){
	return <Awning change={change.bind(this)} />;
  }
  if (state.selected.type === "screen"){
	return <Screen change={change.bind(this)} />;
  }
  if (state.selected.type === "terrace"){
	return <Terrace change={change.bind(this)} />;
  }
};

export default Step2;

const Awning = ({ change }) => {
  return (
    <div>
      <h2>Markis</h2>
      {"Arm: "}
      <input type="number" min="0" name="arm" onChange={change.bind(this)} />
    </div>
  );
};

const Screen = ({ change }) => {
  <div>
    <h2>Screen</h2>
    {"Totalhöjd: "}
    <input type="number" min="0" name="height" onChange={change.bind(this)} />
  </div>;
};

const Terrace = ({ change }) => {
  <div>
    <h2>Terrass</h2>
    {"Totalt utfall: "}
    <input type="number" min="0" name="width" onChange={change.bind(this)} />
  </div>;
};
