node_modules:
	npm install

build: node_modules
	npm run build

deploy: build
	./node_modules/.bin/surge build sunshade.surge.sh

